import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

import { createStore, applyMiddleware, compose, combineReducers } from 'redux'

import thunk from 'redux-thunk'

// import redux provider
import { Provider } from 'react-redux'

// import our reducer...
import reducer from './reducers/reducer';
import otherReducer from './reducers/otherReducer';

// combined all reducer to a rootReducer.
const rootReducer = combineReducers({
    name: reducer,
    wished: otherReducer
})

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

// Create a redux store...
const store = createStore(
    rootReducer,
    { name: 'Ahmed', wished: 'Play Football' },
    composeEnhancers(applyMiddleware(thunk))
)

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
);


serviceWorker.unregister();
