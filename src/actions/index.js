export const change_name_action = (name) => {
    return {
        type: "CHANGE_NAME",
        payload: name
    }
}

export const change_wished_action = (data) => {
    return {
        type: "CHANGE_HOBBIES",
        payload: data
    }
}

export const get_online_name = () => {
    return (dispatch) => {
        fetch('https://jsonplaceholder.typicode.com/users')
            .then(res => res.json())
            .then(res => {
                dispatch({ type: 'CHANGE_NAME', payload: res[0].name })
                dispatch({ type: 'CHANGE_HOBBIES', payload: 'Nothing' })
            })
    }
}