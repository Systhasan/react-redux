import React from 'react'

import { connect } from 'react-redux'

function addWished(props) {
    return (
        <div>
            <h1> Name is: {props.get_name}</h1>
        </div>
    )
}

const mapStateToProps = (state) => {
    return {
        get_name: state.name
    }
}

export default connect(mapStateToProps, null)(addWished)
