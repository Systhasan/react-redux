import React from 'react';
import AddWished from './components/addWished';
import { connect } from 'react-redux';
import { change_name_action, change_wished_action, get_online_name } from './actions';

function App(props) {
  return (
    <div className="App">
      <header className="App-header">
        <h1>App Component</h1>
        <h1>Hey,  {props.my_name}, </h1>
        <p> and wished is: {props.my_hobies}</p>
        <button onClick={() => {
          props.changeName('Mr. Hasan');
          props.changeWished('Coding')
        }
        }>Get Name
        </button>
        <button onClick={() => { props.changeNameAsync() }}>
          Get Name Async
        </button>
        <AddWished />
      </header>
    </div>
  );
}

// using this we will get store data. which is stored on reducer. 
const mapStateToProps = (state) => {
  return {
    my_name: state.name,
    my_hobies: state.wished
  }
}

// Dispatch list, 
const mapDispatchToProps = (dispatch) => {
  return {
    changeName: (name) => { dispatch(change_name_action(name)) },
    changeWished: (name) => { dispatch(change_wished_action(name)) },
    changeNameAsync: () => { dispatch(get_online_name()) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
