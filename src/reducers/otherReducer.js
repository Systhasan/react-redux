const otherReducer = (state = '', action) => {
    if (action.type === 'CHANGE_HOBBIES') {
        return action.payload
    }
    return state;
}

export default otherReducer;